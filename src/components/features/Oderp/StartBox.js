import React from "react";
import { useRef, useEffect, useState } from "react";
import { Box } from "@mui/system";
import IconButton from "../../utils/IconButton";
import { Tooltip ,TextField} from "@mui/material";
import RestartAltIcon from "@mui/icons-material/RestartAlt";
import useSound from "use-sound";
import { SOUND_MAP } from "../sound/sound";
import KeyboardAltOutlinedIcon from '@mui/icons-material/KeyboardAltOutlined';
import Button from '@mui/material/Button';
import { createTheme, ThemeProvider } from '@mui/material/styles';


const theme = createTheme({
  palette: {
    secondary: {
      // This is green.A700 as hex.
      main: '#FFFFFF',
      fontFamily: "Roboto Slab",
    },
  },
});

const StartBox = ({soundType, soundMode, toggleStartScreen, isStartMode}) => {

  const keyboardRef = useRef();
  const [inputChar, setInputChar] = useState("");
  const [correctCount, setCorrectCount] = useState(0);
  const [incorrectCount, setIncorrectCount] = useState(0);
  const [play] = useSound(SOUND_MAP[soundType], {volume: 0.5});

  const getModeButtonClassName = (mode) => {
    if (mode) {
      return "zen-button";
    }
    return "zen-button-deactive";
  };

  const accuracy =
    correctCount + incorrectCount === 0
      ? 0
      : Math.floor((correctCount / (correctCount + incorrectCount)) * 100);
  const keys = [..." abcdefghijklmnopqrstuvwxyz "];
  const resetStats = () => {
    setCorrectCount(0);
    setIncorrectCount(0);
  };

  useEffect(() => {
    keyboardRef.current && keyboardRef.current.focus();
  });
  const handleInputBlur = (event) => {
    keyboardRef.current && keyboardRef.current.focus();
  };
  const handleKeyDown = (event) => {
    if (soundMode){
      play();
    }
    setInputChar(event.key);
    event.preventDefault();
    return;
  };
  const handleKeyUp = (event) => {
    setInputChar("");
    if (event.key === randomKey) {
      let newRandom = getRandomKeyIndex();
      let newKey = keys[newRandom];
      if (newKey === randomKey) {
        if (newRandom === 0 || newRandom === keys.length - 1) {
          newRandom = 1;
        } else {
          newRandom = newRandom + 1;
        }
      }
      setRandomKey(keys[newRandom]);
      setCorrectCount(correctCount + 1);
      return;
    }

    setIncorrectCount(incorrectCount + 1);

    event.preventDefault();
    return;
  };
  const getRandomKeyIndex = () => {
    return Math.floor(Math.random() * 27);
  };

  const [randomKey, setRandomKey] = useState(() => {
    return keys[getRandomKeyIndex()];
  });

  const getClassName = (keyString) => {
    if (keyString !== randomKey) {
      if (inputChar !== "" && inputChar === keyString) {
        return "UNITKEY VIBRATE-ERROR";
      }
      return "UNITKEY";
    }
    if (inputChar !== "" && inputChar === keyString) {
      return "UNITKEY NOVIBRATE-CORRECT";
    }
    return "UNITKEY VIBRATE";
  };
  const getSpaceKeyClassName = () => {
    if (" " !== randomKey) {
      if (inputChar !== "" && inputChar === " ") {
        return "SPACEKEY VIBRATE-ERROR";
      }
      return "SPACEKEY";
    }
    if (inputChar !== "" && inputChar === " ") {
      return "SPACEKEY NOVIBRATE-CORRECT";
    }
    return "SPACEKEY VIBRATE";
  };

  return (
    <ThemeProvider theme={theme}>
    <div>
      <div className="keyboard">
        {/* <IconButton onClick={toggleStartScreen}>
          <Tooltip>
            <span className={getModeButtonClassName(isStartMode)}>
              Эхлэх
            </span>
          </Tooltip>
        </IconButton> */}
        <TextField className="loginText" color="secondary" label="Дотоод код" focused/>
        <Tooltip title="Шинээр эхлэх" placement="bottom">
          <Button color="secondary" fontFamily= "Roboto Slab" variant="outlined" onClick={toggleStartScreen}>Нэвтрэх</Button>
        </Tooltip>
      </div>
    </div>
    </ThemeProvider>
  );
};

export default StartBox;
