import React from "react";
import KeyboardAltIcon from "@mui/icons-material/KeyboardAlt";

const Logo = ({ isFocusedMode, isMusicMode }) => {
  return (
    <div className="header" style={{visibility: isFocusedMode ? 'hidden' : 'visible' }}>
      <h1>
      <KeyboardAltIcon fontSize="large" /> Гацуурт бичээч <KeyboardAltIcon fontSize="large" />
      </h1>
      <span className="sub-header">
        их ажлын дундуурх бяцхан амралт :)
      </span>
    </div>
  );
};

export default Logo;
